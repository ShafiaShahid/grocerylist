# GroceryList

## Command to start interactive shell:
The provided CSV has been uploaded here: 
    <strong>inventory/inventory.csv</strong>

Run:     `node app.js inventory.csv `


## Command to run command list
Provide correct absolute path to the file containing the command list

Run:  `npm run inventory.csv <filepath>` 

Example: `node app.js inventory.csv /Users/shafiashahid/Assignment/file_input.txt`