const fs = require('fs');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

// Parsing the inventory CSV
let data = fs.readFileSync('inventory/inventory.csv').toString()
let dataArray = data.split(/\r?\n/);

let dataJSON = dataArray.map(el => {
    el = el.split(',')
    var obj = {};
    obj[el[0]] = {
        'price': el[1],
        'quantity': el[2]
    }
    return obj
})

let products = dataJSON.reduce((a, b) => Object.assign(a, b), {}) // reducing and converting array to obj
let args = process.argv
let cart = {};
let offers = ['buy_2_get_1_free', 'buy_1_get_half_off'];
let offerCart = {};

// main function dealing with all computation
const getReceipt = (answer) => {
    let orderDtl = {
        subTotal: 0.00,
        discount: 0.00,
        total: 0.00
    }

    if (answer == 'checkout') {
        if (Object.keys(cart).length !== 0) {
            console.log('done')
            return readline.close(); //closing RL and returning from function.
        } else {
            console.log('empty cart')
        }
    } //we need some base case, for recursion
    else if (answer.includes('add')) {
        let inputProduct = answer.split(' ')[1];
        let inputQuantity = parseInt(answer.split(' ')[2], 10);
        if (Object.keys(products).includes(inputProduct) && inputQuantity <= products[inputProduct].quantity) {// checking if the inventory has the input product 
            products[inputProduct].quantity -= inputQuantity
            console.log(answer.replace('add', 'added'));

            if (Object.keys(cart).includes(inputProduct)) {
                cart[inputProduct] += inputQuantity
            } else {
                cart[inputProduct] = inputQuantity
            }
        } else {
            console.log('Product out of stock');
        }
    } else if (answer.includes('offer')) {
        let inputProduct = answer.split(' ')[2];
        let inputOffer = answer.split(' ')[1];
        if (Object.keys(products).includes(inputProduct) && offers.includes(inputOffer)) {// checking if the inventory has the input product 

            if (Object.keys(cart).includes(inputProduct)) {
                offerCart[inputProduct] = inputOffer
            } else {
                offerCart[inputProduct] = inputOffer
            }
            console.log('offer added')

        } else {
            console.log('Invalid offer')
        }
    } else if (answer.includes('bill')) {
        let itemTotal = 0.00;
        Object.keys(cart).forEach(item => {
            itemTotal += products[item].price * cart[item]
        })
        orderDtl.subTotal = itemTotal;
        if (offerCart !== null) {
            // calculating discount here
            Object.keys(offerCart).forEach(item => {
                if (offerCart[item] === 'buy_2_get_1_free' && cart[item]) {
                    let discountPrice = Math.floor(cart[item] / 3) * products[item].price;
                    orderDtl.discount += discountPrice;
                } else if (offerCart[item] === 'buy_1_get_half_off' && cart[item]) {
                    let discountPrice = Math.floor(cart[item] / 2) * products[item].price / 2;
                    orderDtl.discount += discountPrice;
                }
            })
        }
        orderDtl.total = orderDtl.subTotal - orderDtl.discount;
        console.log(`subtotal: ${orderDtl.subTotal}, discount: ${orderDtl.discount}, total: ${orderDtl.total}`);

    }
}


// commands file path is provided as a second argument
if (args.length > 3) {
    try {
        let data = fs.readFileSync(args[3]).toString()
        data = data.split(/\r?\n/);
        console.log(data)
        data.forEach(answer => {
            console.log(answer)
            getReceipt(answer)
        })

    } catch (e) {
        if (e.errno == -2) { console.log('Please specify correct filepath. ') }
        readline.close()
    }
}
// only the inventory argument is provided
else if (args.length <= 3) {
    try {
        var recursiveAsyncReadLine = function () {
            readline.question('$ ', function (answer) {
                getReceipt(answer)
                recursiveAsyncReadLine(); //Calling this function again to ask new question
            });
        }

        recursiveAsyncReadLine()
    } catch (e) {
        readline.close()
    }

}


